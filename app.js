const express = require('express');
const body_parser = require('body-parser');

const userRouter= require('./routers/user.js');
const fundCollectRouter = require('./routers/collect.js');
const materialRouter = require('./routers/material.js');
const eventRouter = require('./routers/event.js');
const locationRouter = require('./routers/location.js');
const forumRouter = require('./routers/forum.js');
const CommenataireRouter = require('./routers/comment.js');

const app = express();
app.use(body_parser.json());
app.use('/User', userRouter);
app.use('/fundCollect', fundCollectRouter);
app.use('/material', materialRouter)
app.use('/event', eventRouter);
app.use('/location', locationRouter);
app.use('/forum', forumRouter);
app.use('/Comment', CommenataireRouter);
module.exports = app;

