const FundCollect = require('../models/collect.js');
const Material = require('../models/material.js');
const User = require('../models/user.js');
const UserService = require('../services/user.js');


class FundCollectService {
  static async createFundCollect(title, amountNeeded, owner, imageUrl, materials) {
    try {
      const newFundCollectionEvent = new FundCollect({ title, amountNeeded, owner ,imageUrl, materials });
      return await newFundCollectionEvent.save();
    } catch (err) {
      throw err;
    }
  }
  static async updateFundCollect(id, updatedData) {
    try {
      return await FundCollect.findByIdAndUpdate(id, updatedData, { new: true });
    } catch (err) {
      throw err;
    }
  }

  static async addMoneyToFundCollect(fundCollectId, amount, userId) {
    try {
      const fundCollect = await FundCollect.findById(fundCollectId);
      if (!fundCollect) {
        throw new Error('Fund collect not found');
      }
      const user = await User.findById(userId);
      if (!user) {
        throw new Error('User not found');
      }
      fundCollect.contributors.push(userId);
      fundCollect.amountCollected += amount;
      return await fundCollect.save();
    } catch (err) {
      throw err;
    }
  }

  static async getAllFundCollect() {
    try {
      const allFundCollect = await FundCollect.find(); 
      return allFundCollect;
    } catch (err) {
      throw err;
    }
  }
  static async getCollectById(id) {
    try {
      const fundCollect = await FundCollect.findById(id); 
      return fundCollect;
    } catch (err) {
      throw err;
    }
  }
  static async addMaterialToFundCollect(fundCollectId, materialIds) {
    try {
      const fundCollect = await FundCollect.findById(fundCollectId);
      if (!fundCollect) {
        throw new Error('Fund collect not found');
      }
      const materialsExist = await Material.find({ _id: { $in: materialIds } });
      if (materialsExist.length !== materialIds.length) {
        throw new Error('One or more materials not found');
      }
      fundCollect.materials.push(...materialIds);
      return await fundCollect.save();
  
    } catch (err) {
      throw err;
    }
  }
  static async addContributorToFundCollect(fundCollectId, contributorIds) {
    try {
      const fundCollect = await FundCollect.findById(fundCollectId);
      if (!fundCollect) {
        throw new Error('Fund collect not found');
      }
      fundCollect.contributors.push(...contributorIds);
      return await fundCollect.save();
  
    } catch (err) {
      throw err;
    }
  }
  static async getMaterialsByFundCollectId(id) {
    try {
      const fundCollect = await FundCollect.findById(id);
      if (!fundCollect) {
        throw new Error('Fund collect not found');
      }

      const materialIds = fundCollect.materials;
      const materials = await Material.find({ _id: { $in: materialIds } });

      return materials;
    } catch (err) {
      throw err;
    }
  }

  static async getContributorsByFundCollectId(id) {
    try {
      const fundCollect = await FundCollect.findById(id);
      if (!fundCollect) {
        throw new Error('Fund collect not found');
      }

      const contributorsIds = fundCollect.contributors;
      const contributors = await User.find({ _id: { $in: contributorsIds } });

      return contributors;
    } catch (err) {
      throw err;
    }
  }

}

module.exports = FundCollectService;
