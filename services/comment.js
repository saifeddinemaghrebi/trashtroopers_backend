// commentService.js
const CommentModel = require('../models/comment.js');

class CommentService {
    static async createComment(content, createdBy, forumId) {
        try {
            const newComment = new CommentModel({ content, createdBy, forumId });
            return await newComment.save();
        } catch (err) {
            throw err;
        }
    }

    static async getAllComments(forumId) {
        try {
            return await CommentModel.find({ forumId });
        } catch (err) {
            throw err;
        }
    }

    static async getCommentById(commentId) {
        try {
            return await CommentModel.findById(commentId);
        } catch (err) {
            throw err;
        }
    }

    static async updateComment(commentId, updates) {
        try {
            return await CommentModel.findByIdAndUpdate(commentId, updates, { new: true });
        } catch (err) {
            throw err;
        }
    }

    static async deleteComment(commentId) {
        try {
            return await CommentModel.findByIdAndDelete(commentId);
        } catch (err) {
            throw err;
        }
    } 
    
    static async getCommentsByForumId(forumId) {
        try {
            return await CommentModel.find({ forumId });
        } catch (err) {
            throw err;
        }
    }
}

module.exports = CommentService;
