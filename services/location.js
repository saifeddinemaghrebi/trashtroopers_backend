// locationService.js
const LocationModel = require('../models/location.js');

class LocationService {
    static async createLocation(latitude, longitude, address) {
        try {
            const newLocation = new LocationModel({ latitude, longitude, address });
            return await newLocation.save();
        } catch (err) {
            throw err;
        }
    }

    static async getAllLocations() {
        try {
            return await LocationModel.find({});
        } catch (err) {
            throw err;
        }
    }

    static async getLocationById(locationId) {
        try {
            return await LocationModel.findById(locationId);
        } catch (err) {
            throw err;
        }
    }

    static async updateLocation(locationId, updatedFields) {
        try {
            return await LocationModel.findByIdAndUpdate(locationId, updatedFields, { new: true });
        } catch (err) {
            throw err;
        }
    }

    static async deleteLocation(locationId) {
        try {
            return await LocationModel.findByIdAndDelete(locationId);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = LocationService;
