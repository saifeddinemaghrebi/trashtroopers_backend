const EventModel = require('../models/event');

class EventService {
    static async createEvent(title, description, locationId, date, ownerId, participants) {
        try {
            const newEvent = new EventModel({ title, description, location: locationId, owner: ownerId, date, participants });
            return await newEvent.save();
        } catch (err) {
            throw err;
        }
    }

    static async getAllEvents() {
        try {
            return await EventModel.find({});
        } catch (err) {
            throw err;
        }
    }

    static async getEventById(eventId) {
        try {
            return await EventModel.findById(eventId);
        } catch (err) {
            throw err;
        }
    }

    static async updateEvent(eventId, updatedFields) {
        try {
            return await EventModel.findByIdAndUpdate(eventId, updatedFields, { new: true });
        } catch (err) {
            throw err;
        }
    }

    static async deleteEvent(eventId) {
        try {
            return await EventModel.findByIdAndDelete(eventId);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = EventService;