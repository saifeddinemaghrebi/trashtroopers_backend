const ForumModel = require('../models/forum.js');

class ForumService {
  static async createForum(title, description, createdBy) {
    try {
      const newForum = new ForumModel({ title, description, createdBy });
      return await newForum.save();
    } catch (err) {
      throw err;
    }
  }
  static async getAllForums() {
    try {
      return await ForumModel.find({});
    } catch (err) {
      throw err;
    }
  }  
  static async getForumById(forumId) {
    try {
      return await ForumModel.findById(forumId);
    } catch (err) {
      throw err;
    }
  }
  static async updateForum(forumId, updates) {
    try {
      return await ForumModel.findByIdAndUpdate(forumId, updates, { new: true });
    } catch (err) {
      throw err;
    }
  }
  static async deleteForum(forumId) {
    try {
      return await ForumModel.findByIdAndDelete(forumId);
    } catch (err) {
      throw err;
    }
  }
} 


module.exports = ForumService;
