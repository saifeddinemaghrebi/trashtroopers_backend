const Material = require('../models/material.js');

class MaterialService {
  static async createMaterial(name, image, price, description) {
    try {
      const newMaterial = new Material({ name, image, price, description });
      return await newMaterial.save();
    } catch (err) {
      throw err;
    }
  }

  static async getAllMaterials() {
    try {
      return await Material.find();
    } catch (err) {
      throw err;
    }
  }

  static async getMaterialById(id) {
    try {
      return await Material.findById(id);
    } catch (err) {
      throw err;
    }
  }

  static async deleteMaterial(id) {
    try {
      return await Material.findByIdAndDelete(id);
    } catch (err) {
      throw err;
    }
  }

  static async updateMaterial(id, updatedData) {
    try {
      return await Material.findByIdAndUpdate(id, updatedData, { new: true });
    } catch (err) {
      throw err;
    }
  }
}

module.exports = MaterialService;

