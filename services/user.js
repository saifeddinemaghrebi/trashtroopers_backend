const UserModel = require('../models/user.js');
const jwt = require('jsonwebtoken');
class UserService{
    static async registerUser(username,email,password){
        try{
            const CreatUser = new UserModel({username,email,password});
            return await CreatUser.save();
        }catch(err){
            throw err;
        }
    }
    static async checkuser(email){
        try{
            return await UserModel.findOne({email});
        }catch(err){
            throw err;
        }
    }
    static async UserById(id){
        try{
            const user =  await UserModel.findOne({_id:id});
            if(user){
                return user;
            }
            else{
                return null ;
            }
        }catch(err){
            throw err;
        }
    }
    static async generateToken(tokenData,secretKey,jwt_expire){
        return jwt.sign(tokenData,secretKey,{expiresIn:jwt_expire});
    }
    static async deleteAllUser() {
        try {
          const resultat = await UserModel.deleteMany();
          console.log(`${resultat.deletedCount} utilisateurs supprimés.`);
          return true;
        } catch (erreur) {
            return false;
          console.error(`Erreur lors de la suppression des utilisateurs : ${erreur.message}`);
        }
      }
}

module.exports = UserService;

