const mongoose = require('mongoose');
const db = require('../config/db');
const LocationModel = require('./location');
const UserModel = require('../models/user');


const {Schema} = mongoose;
const eventSchema = new Schema({
  title: {
    type: String,
    required : true,
  },
  description:{
    type : String,
  },
  location: {
    type: Schema.Types.ObjectId,
    ref: 'Location',
  },
  date: {
    type: Date,
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  participants: [{
    type: Schema.Types.ObjectId,
    ref: 'user',
  }],
    
});

const EventModel = db.model('event', eventSchema);

module.exports = EventModel;