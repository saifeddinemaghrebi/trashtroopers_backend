const mongoose = require('mongoose');
const db = require('../config/db');
const bcrypt = require("bcrypt");

const {Schema} = mongoose; 
const forumSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    }, 
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    
});

const ForumModel = db.model('forum', forumSchema);
module.exports = ForumModel;