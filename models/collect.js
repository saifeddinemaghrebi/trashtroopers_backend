const mongoose = require('mongoose');
const db = require('../config/db');

const fundCollectSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  amountNeeded: {
    type: Number,
    required: true
  },
  amountCollected: {
    type: Number,
    default: 0.0
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  imageUrl: String,  
  contributors: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  materials: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Material' }],
});

const FundCollect =  db.model('FundCollect', fundCollectSchema);
module.exports = FundCollect;