// location.js
const mongoose = require('mongoose');
const db = require('../config/db');
const { Schema } = mongoose;

const locationSchema = new Schema({
    latitude: {
        type: Number,
    },
    longitude: {
        type: Number,
    },
    address: {
        type: String,
    },
    // Additional attributes for location
});

const LocationModel = db.model('Location', locationSchema);

module.exports = LocationModel;