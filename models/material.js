const mongoose = require('mongoose');
const db = require('../config/db');

const materialSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  image: {
    type: String, 
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

const Material = db.model('Material', materialSchema);

module.exports = Material;
