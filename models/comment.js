// comment.js
const mongoose = require('mongoose');
const db = require('../config/db');

const { Schema } = mongoose;

const commentSchema = new Schema({
    content: {
        type: String,
        required: true,
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    forumId: {
        type: Schema.Types.ObjectId,
        ref: 'forum',
        required: true,
    },
    // You can add more fields as needed, e.g., timestamps, likes, etc.
});

const CommentModel = db.model('comment', commentSchema);

module.exports = CommentModel;
