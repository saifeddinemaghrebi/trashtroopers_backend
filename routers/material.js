const express = require('express');
const router = express.Router();
const MaterialController = require("../controllers/material.js");

router.post('/create', MaterialController.createMaterial);
router.get('/getAll', MaterialController.getAllMaterials);
router.get('/getById/:id', MaterialController.getMaterialById);
router.delete('/delete/:id', MaterialController.deleteMaterial);
router.put('/update/:id', MaterialController.updateMaterial);

module.exports = router;
