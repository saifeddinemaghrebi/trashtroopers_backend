const router = require('express').Router();
const UserController = require("../controllers/user.js");
router.post('/registration',UserController.register);
router.post('/login',UserController.login);
router.post('/getById',UserController.GetUserById);
router.delete('/fasakhLkol',UserController.deliteAll);

module.exports = router;