const router = require('express').Router();
const EventController = require('../controllers/event.js');

// Routes for events
router.post('/create', EventController.createEvent);
router.get('/getAll', EventController.getAllEvents);
router.get('/:id', EventController.getEventById);
router.put('/:id', EventController.updateEvent);
router.delete('/:id', EventController.deleteEvent);

module.exports = router;