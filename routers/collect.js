const router = require('express').Router();
const FundCollectController = require("../controllers/collect.js");

router.post('/create', FundCollectController.createFundCollect);
router.post('/addMoney/:id', FundCollectController.addMoneyToFundCollect);
router.post('/addMaterial/:id', FundCollectController.addMaterialToFundCollect);
router.post('/addContributor/:id', FundCollectController.addContributorToFundCollect);
router.put('/update/:id', FundCollectController.updateFundCollect);
router.get('/getAll', FundCollectController.getAllFundCollect);
router.get('/getById/:id', FundCollectController.getCollectById);
router.get('/getMaterials/:id', FundCollectController.getMaterialsByFundCollectId);
router.get('/getContributors/:id', FundCollectController.getContributorsByFundCollectId);

module.exports = router;