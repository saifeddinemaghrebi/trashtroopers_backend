const express = require('express');
const router = express.Router();
const LocationController = require('../controllers/location');

// Routes for locations
router.post('/create', LocationController.createLocation);
router.get('/getAll', LocationController.getAllLocations);
router.get('/:id', LocationController.getLocationById);
router.put('/:id', LocationController.updateLocation);
router.delete('/:id', LocationController.deleteLocation);

module.exports = router;