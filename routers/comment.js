// commentRoutes.js
const router = require('express').Router();
const CommentController = require('../controllers/comment.js');

router.post('/create', CommentController.createComment);
router.get('/getAll/:forumId', CommentController.getAllComments);
router.get('/byForumId/:forumId', CommentController.getCommentsByForumId);
//router.get('/:commentId', CommentController.getCommentById);
//router.put('/:commentId', CommentController.updateComment);
//router.delete('/:commentId', CommentController.deleteComment);

module.exports = router;
