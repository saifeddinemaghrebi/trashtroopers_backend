const router = require('express').Router();

const ForumController = require('../controllers/forum.js');
router.post('/create', ForumController.createForum);
router.get('/getAll', ForumController.getAllForums);
// Lire un forum par ID
router.get('/:forumId', ForumController.getForumById);
router.put('/:forumId', ForumController.updateForum);
router.delete('/:forumId', ForumController.deleteForum);

module.exports = router; 


