const UserService = require('../services/user.js');
exports.register = async(req,res,next)=>{
    try{
const {username,email,password}=req.body;


const user = await UserService.checkuser(email);
if(user){

    res.status(404).json({status:false,message:'Email Already Percissted',statusCode:402});
    return ;
    
}
else{
    const successRes =await UserService.registerUser(username,email,password);
    res.json({status:true,success:"User Registered Successfully"});
    return ;
    
}
}catch(err){
    throw err;
}

}
exports.login = async(req,res,next)=>{
    try{
const {username,email,password}=req.body;
const user = await UserService.checkuser(email);
if(!user){

    res.status(402).json({status:false,message:'User dont exist',statusCode:402});
    return ;
 

}
const isMatch = await user.comparePassword(password);
if(isMatch == false){
    res.status(403).json({status:false,message:'Wrong password',statusCode:403});
    return ;
    
}

let tokenData ={username:user.username,_id:user._id,email:user.email};
const token = await UserService.generateToken(tokenData,"secretkey",'24h');
res.status(200).json({status:true,token:token});

}catch(err){
    throw err;
}
 
}
exports.GetUserById=async(req,res,next)=>{
    const id=req.body.id;
    console.log(id);
    const user = await UserService.UserById(id);
    try {
        if(user!=null){
            res.status(200).json({status:true,email:user.email,username:user.username});
        }
        else{
            res.status(403).json({status:false,message:"User Not Found"});
        }
    } catch (error) {
        throw(error);
    }
    
}
exports.deliteAll=async(req,res,next)=>{
    const resultat = await UserService.deleteAllUser();
    if(resultat){
        res.status(200).json({status:true,message:"all User deleted Succefully"});
    }
    res.status(400).json({status:false,message:"unkown Error"});

}

