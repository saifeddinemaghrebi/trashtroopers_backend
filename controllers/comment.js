// commentController.js
const CommentService = require('../services/comment.js');

exports.createComment = async (req, res, next) => {
    try {
        const { content, createdBy, forumId } = req.body;

        const comment = await CommentService.createComment(content, createdBy, forumId);
        res.json({ status: true, comment });
    } catch (err) {
        next(err);
    }
};

exports.getAllComments = async (req, res, next) => {
    try {
        const { forumId } = req.params;
        const comments = await CommentService.getAllComments(forumId);
        res.json({ status: true, comments });
    } catch (err) {
        next(err);
    }
}; 
exports.getCommentsByForumId = async (req, res, next) => {
    try {
        const { forumId } = req.params;
        const comments = await CommentService.getCommentsByForumId(forumId);
        res.json({ status: true, comments });
    } catch (err) {
        next(err);
    }
};

// Similar implementations for getCommentById, updateComment, and deleteComment
