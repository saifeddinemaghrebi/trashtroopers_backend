const FundCollectService = require('../services/collect.js');

exports.createFundCollect = async (req, res, next) => {
  try {
    const { title, amountNeeded, owner, imageUrl, materials } = req.body;

    const successRes = await FundCollectService.createFundCollect(title, amountNeeded, owner, imageUrl,materials);
    res.json({ status: true, success: "Fund collection created successfully" });
  } catch (err) {
    next(err);
  }
};

exports.addMoneyToFundCollect = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { amount, userId } = req.body; 

    const updatedEvent = await FundCollectService.addMoneyToFundCollect(id, amount, userId);
    res.json({ status: true, success: "Money added to fund collection successfully", updatedEvent });
  } catch (err) {
    next(err);
  }
};

exports.getAllFundCollect = async (req, res, next) => {
  try {
    const allFundCollect = await FundCollectService.getAllFundCollect();
    res.json({Collect: allFundCollect});
  } catch (err) {
    next(err);
  }
};

exports.getCollectById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const fundCollect = await FundCollectService.getCollectById(id);

    if (!fundCollect) {
      return res.status(404).json({ status: false, error: 'Fund collection not found' });
    }

    res.json({ status: true, fundCollection: fundCollect });
  } catch (err) {
    next(err);
  }
};

exports.updateFundCollect = async (req, res, next) => {
  try {
    const { id } = req.params;
    const updatedData = req.body;

    const updatedFundCollect = await FundCollectService.updateFundCollect(id, updatedData);

    if (!updatedFundCollect) {
      return res.status(404).json({ status: false, error: 'Fund collection not found' });
    }

    res.json({ status: true, success: 'Fund collection updated successfully', updatedFundCollect });
  } catch (err) {
    next(err);
  }
};

exports.addMaterialToFundCollect = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { materialId } = req.body;

    const updatedFundCollect = await FundCollectService.addMaterialToFundCollect(id, materialId);

    if (!updatedFundCollect) {
      return res.status(404).json({ status: false, error: 'Fund collection not found' });
    }

    res.json({ status: true, success: 'Material added to fund collection successfully', updatedFundCollect });
  } catch (err) {
    next(err);
  }
};
exports.addContributorToFundCollect = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { contributorId } = req.body;
    
    const updatedFundCollect = await FundCollectService.addContributorToFundCollect(id, contributorId);
    if (!updatedFundCollect) {
      return res.status(404).json({ status: false, error: 'Fund collection not found' });
    }

    res.json({ status: true, success: 'Contributor added to fund collection successfully', updatedFundCollect });
  } catch (err) {
    next(err);
  }
};
exports.getMaterialsByFundCollectId = async (req, res, next) => {
  try {
    const { id } = req.params;
    
    const materials = await FundCollectService.getMaterialsByFundCollectId(id);

    res.json({ status: true, materials });
  } catch (err) {
    next(err);
  }
};
exports.getContributorsByFundCollectId = async (req, res, next) => {
  try {
    const { id } = req.params;
    
    const contributors = await FundCollectService.getContributorsByFundCollectId(id);

    res.json({ status: true, contributors });
  } catch (err) {
    next(err);
  }
};
