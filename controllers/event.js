const EventService = require('../services/event.js');

exports.createEvent = async (req, res, next) => {
    try {
        const { title, description, location, date, owner } = req.body;

        const successRes = await EventService.createEvent(title, description, location, date, owner);
        res.status(201).json({ status: true, success: "Event Created Successfully", data: successRes });
    } catch (err) {
        console.error(err);
        res.status(500).json({ status: false, error: "Internal Server Error", details: err.message });
    }
};

exports.getAllEvents = async (req, res, next) => {
    try {
        const events = await EventService.getAllEvents();
        res.json({ status: true, data: events });
    } catch (err) {
        console.error(err);
        res.status(500).json({ status: false, error: "Internal Server Error", details: err.message });
    }
};

exports.getEventById = async (req, res, next) => {
    try {
        const eventId = req.params.id;
        const event = await EventService.getEventById(eventId);
        if (!event) {
            res.status(404).json({ status: false, error: "Event not found" });
            return;
        }
        res.json({ status: true, data: event });
    } catch (err) {
        console.error(err);
        res.status(500).json({ status: false, error: "Internal Server Error", details: err.message });
    }
};

exports.updateEvent = async (req, res, next) => {
    try {
        const eventId = req.params.id;
        const updatedFields = req.body;

        const updatedEvent = await EventService.updateEvent(eventId, updatedFields);
        if (!updatedEvent) {
            res.status(404).json({ status: false, error: "Event not found" });
            return;
        }
        res.json({ status: true, success: "Event Updated Successfully", data: updatedEvent });
    } catch (err) {
        console.error(err);
        res.status(500).json({ status: false, error: "Internal Server Error", details: err.message });
    }
};

exports.deleteEvent = async (req, res, next) => {
    try {
        const eventId = req.params.id;

        const deletedEvent = await EventService.deleteEvent(eventId);
        if (!deletedEvent) {
            res.status(404).json({ status: false, error: "Event not found" });
            return;
        }
        res.json({ status: true, success: "Event Deleted Successfully", data: deletedEvent });
    } catch (err) {
        console.error(err);
        res.status(500).json({ status: false, error: "Internal Server Error", details: err.message });
    }
};
