const LocationService = require('../services/location');

exports.createLocation = async (req, res) => {
    try {
        const { latitude, longitude, address } = req.body;
        const successRes = await LocationService.createLocation(latitude, longitude, address);
        res.status(201).json({ status: true, success: "Location Created Successfully", data: successRes });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: false, error: 'Internal Server Error', details: error.message });
    }
};

exports.getAllLocations = async (req, res) => {
    try {
        const locations = await LocationService.getAllLocations();
        res.json({ status: true, data: locations });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: false, error: 'Internal Server Error', details: error.message });
    }
};

exports.getLocationById = async (req, res) => {
    try {
        const locationId = req.params.id;
        const location = await LocationService.getLocationById(locationId);
        if (!location) {
            return res.status(404).json({ status: false, error: 'Location not found' });
        }
        res.json({ status: true, data: location });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: false, error: 'Internal Server Error', details: error.message });
    }
};

exports.updateLocation = async (req, res) => {
    try {
        const locationId = req.params.id;
        const updatedFields = req.body;

        const updatedLocation = await LocationService.updateLocation(locationId, updatedFields);
        if (!updatedLocation) {
            return res.status(404).json({ status: false, error: 'Location not found' });
        }

        res.json({ status: true, data: updatedLocation });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: false, error: 'Internal Server Error', details: error.message });
    }
};

exports.deleteLocation = async (req, res) => {
    try {
        const locationId = req.params.id;

        const deletedLocation = await LocationService.deleteLocation(locationId);
        if (!deletedLocation) {
            return res.status(404).json({ status: false, error: 'Location not found' });
        }

        res.json({ status: true, data: deletedLocation });
    } catch (error) {
        console.error(error);
        res.status(500).json({ status: false, error: 'Internal Server Error', details: error.message });
    }
};
