const ForumService = require('../services/forum.js');

exports.createForum = async (req, res, next) => {
  try {
    const { title, description, createdBy } = req.body;

    const forum = await ForumService.createForum(title, description, createdBy);
    res.json({ status: true, forum });
  } catch (err) {
    next(err);
  }
} 
exports.getAllForums = async (req, res, next) => {
    try {
      const forums = await ForumService.getAllForums();
      res.json({ status: true, forums });
    } catch (err) {
      next(err);
    }
  } 
  exports.getForumById = async (req, res, next) => {
    try {
      const { forumId } = req.params;
      const forum = await ForumService.getForumById(forumId);
  
      if (!forum) {
        res.status(404).json({ status: false, message: 'Forum not found' });
      } else {
        res.json({ status: true, forum });
      }
    } catch (err) {
      next(err);
    }
  }
  exports.updateForum = async (req, res, next) => {
    try {
      const { forumId } = req.params;
      const updates = req.body;
  
      const updatedForum = await ForumService.updateForum(forumId, updates);
  
      if (!updatedForum) {
        res.status(404).json({ status: false, message: 'Forum not found' });
      } else {
        res.status(200).json({ status: true, updatedForum });
      }
    } catch (err) {
      next(err);
    }
  }
  exports.deleteForum = async (req, res, next) => {
    try {
      const { forumId } = req.params;
      const deletedForum = await ForumService.deleteForum(forumId);
  
      if (!deletedForum) {
        res.status(404).json({ status: false, message: 'Forum not found' });
      } else {
        res.status(204).json({ status: true, message: 'Forum deleted successfully' });
      }
    } catch (err) {
      next(err);
    }
  }
