const MaterialService = require('../services/material.js');

class MaterialController {
    static async createMaterial(req, res, next) {
      try {
        const { name, image, price, description } = req.body;
  
        const newMaterial = await MaterialService.createMaterial(name, image, price, description);
        res.json({ status: true, success: "Material created successfully", newMaterial });
      } catch (err) {
        next(err);
      }
    }
  
    static async getAllMaterials(req, res, next) {
      try {
        const allMaterials = await MaterialService.getAllMaterials();
        res.json({ status: true, materials: allMaterials });
      } catch (err) {
        next(err);
      }
    }
  
    static async getMaterialById(req, res, next) {
      try {
        const { id } = req.params;
  
        const material = await MaterialService.getMaterialById(id);
  
        if (!material) {
          return res.status(404).json({ status: false, error: 'Material not found' });
        }
  
        res.json({ status: true, material });
      } catch (err) {
        next(err);
      }
    }
  
    static async deleteMaterial(req, res, next) {
      try {
        const { id } = req.params;
  
        const deletedMaterial = await MaterialService.deleteMaterial(id);
  
        if (!deletedMaterial) {
          return res.status(404).json({ status: false, error: 'Material not found' });
        }
  
        res.json({ status: true, success: 'Material deleted successfully', deletedMaterial });
      } catch (err) {
        next(err);
      }
    }
  
    static async updateMaterial(req, res, next) {
      try {
        const { id } = req.params;
        const updatedData = req.body;
  
        const updatedMaterial = await MaterialService.updateMaterial(id, updatedData);
  
        if (!updatedMaterial) {
          return res.status(404).json({ status: false, error: 'Material not found' });
        }
  
        res.json({ status: true, success: 'Material updated successfully', updatedMaterial });
      } catch (err) {
        next(err);
      }
    }
  }
  
  module.exports = MaterialController;